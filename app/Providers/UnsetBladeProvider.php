<?php

namespace App\Providers;

use Blade;
use Illuminate\Support\ServiceProvider;

class UnsetBladeProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        Blade::directive('unset', function($expression) {
            return "<?php unset{$expression}; ?>";
        });
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {}
}
